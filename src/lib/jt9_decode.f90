! This source code file was last time modified by Igor UA3DJY on April 3rd, 2017.
! All changes are shown in the patch file coming together with the full JTDX source code.

module jt9_decode

  type :: jt9_decoder
     procedure(jt9_decode_callback), pointer :: callback
   contains
     procedure :: decode
  end type jt9_decoder

  abstract interface
     subroutine jt9_decode_callback (this, utc, sync, snr, dt, freq, drift, decoded, servis9)
       import jt9_decoder
       implicit none
       class(jt9_decoder), intent(inout) :: this
       integer, intent(in) :: utc
       real, intent(in) :: sync
       integer, intent(in) :: snr
       real, intent(in) :: dt
       real, intent(in) :: freq
       integer, intent(in) :: drift
       character(len=22), intent(in) :: decoded
       character(len=1), intent(in) ::  servis9
     end subroutine jt9_decode_callback
  end interface

contains

  subroutine decode(this,callback,ss,nutc,nfqso,newdat,npts8,nfa,nfsplit,nfb,ntol,  &
       nzhsym,nagain,nagainfil,ndepth,nmode)
    use timer_module, only: timer
!npts8 = 74736
    include 'constants.f90'
    class(jt9_decoder), intent(inout) :: this
    procedure(jt9_decode_callback) :: callback
    real ss(184,NSMAX)
    logical, intent(in) :: newdat,nagain
    logical(1), intent(in) :: nagainfil
    character*22 msg
    real*4 ccfred(NSMAX)
    real*4 red2(NSMAX)
    logical ccfok(NSMAX)
    logical done(NSMAX)
    integer*1 i1SoftSymbols(207)
    character servis9*1
    logical(1) freemsg,dupe
    type accepted_decode
       integer i
       character*22 decoded
    end type accepted_decode
    type(accepted_decode) dec(200)
    save ccfred,red2

    this%callback => callback
    ndecoded=0
    nsps=6912                                   !Params for JT9-1
    df3=1500.0/2048.0
    dec%i=-1; dec%decoded=''

    tstep=0.5*nsps/12000.0                      !Half-symbol step (seconds)
    done=.false.; dupe=.false.

    nf0=0
    nf1=nfa

    if(nmode.eq.65+9) nf1=nfsplit
    if(nagain .or. nagainfil) then
       if(nfqso.ge.21) nf1=nfqso-20
       if(nfqso.lt.21) nf1=nfqso
       if(nfqso.le.4962) nfb=nfqso+20
       if(nfqso.gt.4962) nfb=nfqso
    endif
    ia=max(1,nint((nf1-nf0)/df3))
    ib=min(NSMAX,nint((nfb-nf0)/df3))
    lag1=-int(2.5/tstep + 0.9999)
    lag2=int(5.0/tstep + 0.9999)
    if(newdat) then
!       call timer('sync9   ',0)
       call sync9(ss,nzhsym,lag1,lag2,ia,ib,ccfred,red2,ipk)
!       call timer('sync9   ',1)
    endif

    ipeak1=0; ipeak2=0; ipeak3=0; ipeak4=0; ipeak5=0
    if(.not.nagain .and. (.not.nagainfil)) then
       if(nfqso.ge.4) infa=nint((nfqso-3)/df3)
       if(nfqso.lt.4) infa=nint((nfqso)/df3)
       if(nfqso.le.4979) infb=nint((nfqso+3)/df3)
       if(nfqso.gt.4979) infb=nint((nfqso)/df3)
       if(infa.gt.0 .and. infb.gt.0 .and. infa.lt.NSMAX .and. infb.lt.NSMAX) then
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax) then
                ipeak1=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1) then
                ipeak2=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1 .and. i.ne.ipeak2) then
                ipeak3=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1 .and. i.ne.ipeak2 .and. i.ne.ipeak3) then
                ipeak4=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1 .and. i.ne.ipeak2 .and. i.ne.ipeak3 .and. i.ne.ipeak4) then
                ipeak5=i
                ccfredmax=ccfred(i)
             endif
          enddo
       endif
    endif
	
    if(nagain .or. nagainfil) then
    ipeak1=0; ipeak2=0; ipeak3=0; ipeak4=0; ipeak5=0
       if(nagain) then
         if(nfqso.ge.4) infa=nint((nfqso-3)/df3)
         if(nfqso.lt.4) infa=nint((nfqso)/df3)
         if(nfqso.le.4979) infb=nint((nfqso+3)/df3)
         if(nfqso.gt.4979) infb=nint((nfqso)/df3)
       endif
       if(nagainfil) then
          if(nfqso.ge.21) infa=nint((nfqso-20)/df3)
          if(nfqso.lt.21) infa=nint((nfqso)/df3)
          if(nfqso.le.4962) infb=nint((nfqso+20)/df3)
          if(nfqso.gt.4962) infb=nint((nfqso)/df3)
       endif
       if(infa.gt.0 .and. infb.gt.0 .and. infa.lt.NSMAX .and. infb.lt.NSMAX) then
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax) then
                ipeak1=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1) then
                ipeak2=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1 .and. i.ne.ipeak2) then
                ipeak3=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1 .and. i.ne.ipeak2 .and. i.ne.ipeak3) then
                ipeak4=i
                ccfredmax=ccfred(i)
             endif
          enddo
          ccfredmax=0.
          do i=infa,infb
             if(ccfred(i).gt.ccfredmax .and. i.ne.ipeak1 .and. i.ne.ipeak2 .and. i.ne.ipeak3 .and. i.ne.ipeak4) then
                ipeak5=i
                ccfredmax=ccfred(i)
             endif
          enddo
       endif
    endif

    nsps8=nsps/8
    df8=1500.0/nsps8
    dblim=db(864.0/nsps8) - 26.2

    ia1=1                         !quel compiler gripe
    ib1=1                         !quel compiler gripe
    do nqd=1,0,-1
       if(nqd.eq.1 .and. nagainfil) cycle
       limit=5000
       ccflim=3.0
       red2lim=1.6
!       schklim=2.2
       if(ndepth.eq.2) then
          limit=10000
          ccflim=2.7
       endif
       if(ndepth.ge.3) then
          limit=30000
          ccflim=2.5
!          schklim=2.0
       endif
       if(nqd.eq.1) then
          limit=200000
          ccflim=2.4
!          schklim=1.8
       endif
       if(nagain) then
          limit=5000000
          ccflim=2.3
       endif
       if(nagainfil) then
          limit=300000
          ccflim=2.3
       endif

       ccfok=.false.

       ntol1=ntol !supress compiler warning
       if(nqd.eq.1) then
!          nfa1=nfqso-ntol
!          nfb1=nfqso+ntol
          nfa1=nfqso-2
          nfb1=nfqso+2
          ia=max(1,nint((nfa1-nf0)/df3))
          ib=min(NSMAX,nint((nfb1-nf0)/df3))
          ccfok(ia:ib)=(ccfred(ia:ib).gt.(ccflim-2.0)) .and.               &
               (red2(ia:ib).gt.(red2lim-1.0))
          ia1=ia
          ib1=ib
       else
          nfa1=nf1
          nfb1=nfb
          ia=max(1,nint((nfa1-nf0)/df3))
          ib=min(NSMAX,nint((nfb1-nf0)/df3))
          do i=ia,ib
             ccfok(i)=ccfred(i).gt.ccflim .and. red2(i).gt.red2lim
          enddo
          ccfok(ia1:ib1)=.false.
       endif

       fgood=0.
       do i=ia,ib
          if((nagain.or.nagainfil.or.nqd.eq.1) .and. i.ne.ipeak1 .and. i.ne.ipeak2 .and. i.ne.ipeak3 &
              .and. i.ne.ipeak4 .and. i.ne.ipeak5) cycle
          if((nagain.or.nagainfil.or.nqd.eq.1) .and. i.eq.ipeak1 .and. ipeak1.eq.0) cycle
          if((nagain.or.nagainfil.or.nqd.eq.1) .and. i.eq.ipeak2 .and. ipeak2.eq.0) cycle
          if((nagain.or.nagainfil.or.nqd.eq.1) .and. i.eq.ipeak3 .and. ipeak3.eq.0) cycle
          if((nagain.or.nagainfil.or.nqd.eq.1) .and. i.eq.ipeak4 .and. ipeak4.eq.0) cycle
          if((nagain.or.nagainfil.or.nqd.eq.1) .and. i.eq.ipeak5 .and. ipeak5.eq.0) cycle

          if(done(i) .or. (.not.ccfok(i))) cycle
          f=(i-1)*df3

          if(nqd.eq.1 .or.                                                   &
               (ccfred(i).ge.ccflim .and. abs(f-fgood).gt.10.0*df8)) then

!             call timer('softsym ',0)
             fpk=nf0 + df3*(i-1)

             call softsym(npts8,nsps8,newdat,fpk,syncpk,snrdb,xdt,    &
                  freq,drift,a3,schk,i1SoftSymbols)
!             call timer('softsym ',1)

             sync=(syncpk+1)/4.0
             if(nqd.eq.1 .and. ((sync.lt.0.5) .or. (schk.lt.1.0))) cycle
             if(nqd.ne.1 .and. ((sync.lt.1.0) .or. (schk.lt.1.5))) cycle

             freemsg=.false.
!             call timer('jt9fano ',0)
             call jt9fano(i1SoftSymbols,limit,nlim,msg,freemsg)
!             call timer('jt9fano ',1)

             servis9=' '
             if(abs(f-float(nfqso)).lt.3.0 .and. freemsg) servis9=','
             if(abs(f-float(nfqso)).ge.3.0 .and. freemsg) servis9='.'

             if(sync.lt.0.0 .or. snrdb.lt.dblim-2.0) sync=0.0
             nsync=int(sync)
             if(nsync.gt.10) nsync=10
             nsnr=nint(snrdb)
             ndrift=nint(drift/df3)

             if(msg.ne.'                      ') then
                dupe=.false.
                if(ndecoded.ne.0) then
                   do k=1,200
                      if(dec(k)%i.eq.-1) exit
                      if(dec(k)%decoded.eq.msg .and. abs(dec(k)%i-i).lt.24) dupe=.true.
                   enddo
                endif

                if(dupe) cycle
 
                if (associated(this%callback)) then
                   call this%callback(nutc,sync,nsnr,xdt,freq,ndrift,msg,servis9)
                end if
                iaa=max(1,i-1)
!                ibb=min(NSMAX,i+22)
                ibb=min(NSMAX,i+11)
                fgood=f
                ndecoded=ndecoded+1
                ccfok(iaa:ibb)=.false.
                done(iaa:ibb)=.true.
                dec(ndecoded)%i=i
                dec(ndecoded)%decoded=msg
             endif
          endif
       enddo
       if(nagain) exit
    enddo

    return
  end subroutine decode
end module jt9_decode
