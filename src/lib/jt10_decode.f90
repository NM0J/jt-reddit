! This source code file was last time modified by Igor UA3DJY on April 29th, 2017
! All changes are shown in the patch file coming together with the full JTDX source code.

module jt10_decode

  type :: jt10_decoder
     procedure(jt10_decode_callback), pointer :: callback
   contains
     procedure :: decode
  end type jt10_decoder

  abstract interface
     subroutine jt10_decode_callback (this, utc, sync, snr, dt, freq, drift, decoded, servis9)
       import jt10_decoder
       implicit none
       class(jt10_decoder), intent(inout) :: this
       integer, intent(in) :: utc
       real, intent(in) :: sync
       integer, intent(in) :: snr
       real, intent(in) :: dt
       real, intent(in) :: freq
       integer, intent(in) :: drift
       character(len=22), intent(in) :: decoded
       character(len=1), intent(in) ::  servis9
     end subroutine jt10_decode_callback
  end interface

contains

  subroutine decode(this,callback,nutc,nfqso,newdatin,npts8,nfa,nfb,ntol,  &
       nzhsym,nagain,nagainfil,ntrials10,ntrialsrxf10,filter,swl,agcc)
    use timer_module, only: timer
    use jt9_mod2
!npts8 = 74736
!    include 'constants.f90'
    class(jt10_decoder), intent(inout) :: this
    procedure(jt10_decode_callback) :: callback
    integer, parameter :: NSMAX10=11520
    logical, intent(in) :: newdatin,nagain
    logical(1), intent(in) :: nagainfil,filter,swl,agcc
    logical newdat
    character*22 msg
    logical done(NSMAX10)
    integer*1 i1SoftSymbols(207)
    character servis9*1
    integer ncand0
    logical(1) freemsg,dupe,agcclast
    type accepted_decode
       integer i
       character*22 decoded
    end type accepted_decode
    type(accepted_decode) dec(200)
    save ncand0,agcclast

    this%callback => callback
    ndecoded=0
    nsps=6912                                   !Params for JT9-1
    df3=12000.0/27648.0
    dec%i=-1; dec%decoded=''
    done=.false.; dupe=.false.
    ntol1=ntol !supress compiler warning
    newdat=newdatin

    call cpu_time(t1)
    if(filter) then; nfa=max(1,nfqso-80); nfb=min(4945,nfqso+80); endif
    if(nagain .or. nagainfil) then; nfa=max(1,nfqso-20); nfb=min(4945,nfqso+20); endif
    ia=max(1,nint(nfa/df3))
    ib=min(NSMAX10,nint(nfb/df3))
    irxf=nint(nfqso/df3)+1

    th_wide=2.6; th_rxf=2.0; th_agn=2.0
    if(nagain .or. nagainfil) then; th_wide=th_agn; th_rxf=th_agn; newdat=.false.; endif
    if((nagain .or. nagainfil) .and. agcc.neqv.agcclast) newdat=.true.
    agcclast=agcc

!       call timer('sync9   ',0)
       call sync10(nzhsym,ia,ib,irxf,th_wide,th_rxf,ncand0,newdat)
!       call timer('sync9   ',1)

    nsps8=nsps/8
!    df8=1500.0/nsps8 ! 1.73611116
    dblim=db(864.0/nsps8) - 26.2

!    fac=ntrials10**2
    facrxf=ntrialsrxf10**2
!print *,'ncand0',ncand0; ncount=0
    do nqd=1,0,-1

       if(nagain .and. nqd.eq.0) cycle
       if(nagainfil .and. nqd.eq.1) cycle
!          limit=100000*fac ! max limit is 20648000
       if(nqd.eq.0) limit=200000*ntrials10
       if(nqd.eq.1) limit=300000*facrxf
       if(nagain) limit=1000000*ntrialsrxf10
       if(nagainfil) limit=500000*facrxf
       if(swl) limit=2*limit
       limitr=limit/4

       if(nqd.eq.1) then
!cycle
          do icand=1,maxcandrxf
!print *,carxf(icand)%i,carxf(icand)%ccfred
             if(carxf(icand)%i.lt.0) cycle
             fpk=(carxf(icand)%i-1)*df3
             if(ncand0.gt.199 .and. icand.gt.2) limit=limitr
!print *,fpk,carxf(icand)%ccfred
!print *,fpk
!             call timer('softsym ',0)
             call softsym10(npts8,nsps8,newdat,fpk,syncpk,snrdb,xdt,    &
                  freq,drift,schk,i1SoftSymbols,qualf,syncro)
!             call timer('softsym ',1)
             sync=(syncpk+1)/4.0
!print *,freq,syncpk,schk
!             if(sync.lt.0.5 .or. schk.lt.1.0) cycle
             if(sync.lt.0.5 .or. schk.lt.2.0 .or. qualf.lt.2.4) cycle
             if(syncro.lt.0.2 .or. syncro.gt.5.) cycle
             freemsg=.false.
!             call timer('jt9fano ',0)
             call jt9fano(i1SoftSymbols,limit,nlim,msg,freemsg)
!             call timer('jt9fano ',1)
             servis9=' '
             if(freemsg) servis9=','
             if(sync.lt.0.0 .or. snrdb.lt.dblim-2.0) sync=0.0
             nsync=int(sync)
             if(nsync.gt.10) nsync=10
             nsnr=nint(snrdb)
             ndrift=nint(drift/df3)

             if(msg.ne.'                      ') then
                if (associated(this%callback)) then
                   call this%callback(nutc,sync,nsnr,xdt,freq,ndrift,msg,servis9)
                end if

                ndecoded=ndecoded+1
!print *,carxf(icand)%ccfred
                done((irxf-3):(irxf+3))=.true.
                dec(ndecoded)%i=carxf(icand)%i
                dec(ndecoded)%decoded=msg
                exit
             endif
          enddo
       endif

       ncurcand=0

       if(nqd.eq.0) then
!exit
          do icand=1,maxcand
!print *,ca(icand)%i,ca(icand)%ccfred
             if(ca(icand)%i.lt.0) cycle; if(done(ca(icand)%i)) cycle
             if(nagainfil .and. icand.gt.10) exit
!print *,done(ca(icand)%i),ccfok(ca(icand)%i)
             ncurcand=ncurcand+1
             fpk=(ca(icand)%i-1)*df3
!print *,fpk
             if((.not.nagainfil .and. ncand0.gt.199 .and. ncurcand.gt.10) .or. ncurcand.gt.15) limit=limitr
!ncount=ncount+1; print *,ncount
!             call timer('softsym ',0)
             call softsym10(npts8,nsps8,newdat,fpk,syncpk,snrdb,xdt,    &
                  freq,drift,schk,i1SoftSymbols,qualf,syncro)
!             call timer('softsym ',1)
             sync=(syncpk+1)/4.0
!print *,freq,syncpk,schk
!             if(sync.lt.1.0 .or. schk.lt.1.5) cycle
             if(sync.lt.1.0 .or. schk.lt.2.0 .or. qualf.lt.2.4) then
                if(ncurcand.gt.0) ncurcand=ncurcand-1
                cycle
             endif
             if(syncro.lt.0.2 .or. syncro.gt.5.) then
                if(ncurcand.gt.0) ncurcand=ncurcand-1
                cycle
             endif
!print *,qualf,syncro
             freemsg=.false.
!             call timer('jt9fano ',0)
             call jt9fano(i1SoftSymbols,limit,nlim,msg,freemsg)
!             call timer('jt9fano ',1)
             servis9=' '
             if(abs(fpk-float(nfqso)).lt.3.0 .and. freemsg) servis9=',' ! keep it for hide freemsg filter
             if(abs(fpk-float(nfqso)).ge.3.0 .and. freemsg) servis9='.'

             if(sync.lt.0.0 .or. snrdb.lt.dblim-2.0) sync=0.0
             nsync=int(sync)
             if(nsync.gt.10) nsync=10
             nsnr=nint(snrdb)
             ndrift=nint(drift/df3)

             if(msg.ne.'                      ') then
                dupe=.false.
                if(ndecoded.ne.0) then
                   do k=1,200
                      if(dec(k)%i.eq.-1) exit
                      if(dec(k)%decoded.eq.msg .and. abs(dec(k)%i-ca(icand)%i).lt.24) dupe=.true.
                   enddo
                endif

                if(dupe) cycle
 
                if(associated(this%callback)) then
                   call this%callback(nutc,sync,nsnr,xdt,freq,ndrift,msg,servis9)
                end if
                ndecoded=ndecoded+1
!print *,schk,qualf
                iaa=max(1,ca(icand)%i-5)
                ibb=min(NSMAX10,ca(icand)%i+5)
                done(iaa:ibb)=.true.
                dec(ndecoded)%i=ca(icand)%i
                dec(ndecoded)%decoded=msg
             endif
             call cpu_time(t2)
             if(.not.swl .and. (t2-t1).gt.12.0) exit
             if(swl .and. (t2-t1).gt.30.0) exit
          enddo
       endif
    enddo

    return
  end subroutine decode
end module jt10_decode
