! This source code file was last time modified by Igor UA3DJY on March 6th, 2017
! All changes are shown in the patch file coming together with the full JTDX source code.

subroutine subtract65(freqa1,dt,a2,nfilt)

! Subtract a jt65 signal
!
! Measured signal  : dd(t)    = a(t)cos(2*pi*f0*t+theta(t))
! Reference signal : cref(t)  = exp( j*(2*pi*f0*t+phi(t)) )
! Complex amp      : cfilt(t) = LPF[ dd(t)*CONJG(cref(t)) ]
! Subtract         : dd(t)    = dd(t) - 2*REAL{cref*cfilt}

  use packjt
!  use timer_module, only: timer
  use jt65_mod2
  use jt65_mod3
  use jt65_mod4
  use jt65_mod6

  integer k,nsym
  integer ns
  integer nref,id,ind
  real*4 window(-nfilt/2:nfilt/2)
  real freqa1,dt,dphi,phi,twopi,omega,a2,x0,x1
  logical(1) first
  data first/.true./
  save first

  twopi=6.28318531
  pi512=1608.49548 !pi*512
! Symbol duration is 4096/11025 s.
! Sample rate is 12000/s, so 12000*(4096/11025)=4458.23 samples/symbol.
  nstart=nint(dt*12000.0)+1
  nsym=126
  ns=4458
  nref=561737 ! nsym*ns+29
  phi=0.0
  ind=1
  isym=1
  id=1

!  call timer('subtr_1 ',0)
  x0=672000.0/2 ! 672000 samples are used for freq drift calculation, centering it
  x1=dt*12000.0+1.0
  do k=1,nsym
    if(prc(k)) then
        omega=(pi512*(freqa1+a2*(k*4458.23+x1-x0)/672000.0))/256 ! =2*pi*f0
    else
        omega=(pi512*(freqa1+a2*(k*4458.23+x1-x0)/672000.0+2.6917*(correct(isym)+2)))/256 ! =2*pi*(f0+...
        isym=isym+1
    endif
    dphi=omega/12000.0
    do i=1,ns
        cref(ind)=cexp(cmplx(0.0,phi))
        phi=mod(phi+dphi,twopi)
        id=nstart-1+ind
        if(id.gt.npts) exit
        if(id.ge.1) camp(ind)=dd(id)*conjg(cref(ind))
        ind=ind+1
    enddo
    if(mod(k,5).eq.0) then !mod
    id=id+1
    if(id.gt.npts) exit
    if(id.ge.1) camp(ind)=dd(id)*conjg(cref(ind))
    ind=ind+1
    endif
  enddo
!  call timer('subtr_1 ',1)

!  call timer('subtr_2 ',0)
! Smoothing filter: do the convolution by means of FFTs. Ignore end-around 
! cyclic effects for now.

  nfft=564480
  if(first) then
! Create and normalize the filter
     sum1=0.0
     do j=-nfilt/2,nfilt/2
        window(j)=cos(twopi*j/(2*nfilt))
!        window(j)=abs(cos(twopi*j/(2*nfilt)))
        sum1=sum1+window(j)
     enddo
     cw=0.
     do l=-nfilt/2,nfilt/2
        j1=l+1
        if(j1.lt.1) j1=j1+nfft
        cw(j1)=window(l)/sum1
     enddo
     call four2a(cw,nfft,1,-1,1)
     first=.false.
  endif

  cfilt(1:nref)=camp(1:nref)
  cfilt(nref+1:nfft)=0.
  call four2a(cfilt,nfft,1,-1,1)
  fac=1.0/float(nfft)
  cfilt(1:nfft)=fac*cfilt(1:nfft)*cw(1:nfft)
  call four2a(cfilt,nfft,1,1,1)
!  call timer('subtr_2 ',1)

! Subtract the reconstructed signal
!  call timer('subtr_3 ',0)
  do m=1,nref
     j2=nstart+m-1
     if(j2.ge.1 .and. j2.le.npts) dd(j2)=dd(j2)-2*REAL(cfilt(m)*cref(m))
  enddo
!  call timer('subtr_3 ',1)

  return
end subroutine subtract65 
