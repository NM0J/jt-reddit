// -*- Mode: C++ -*-
// This source code file was last time modified by Arvo ES1JA on December 26th, 2016
// All changes are shown in the patch file coming together with the full JTDX source code.
#ifndef LogQSO_H
#define LogQSO_H

#ifdef QT5
#include <QtWidgets>
#else
#include <QtGui>
#endif

#include <QScopedPointer>

#include "Radio.hpp"

namespace Ui {
  class LogQSO;
}

class QSettings;

class LogQSO : public QDialog
{
  Q_OBJECT

public:
  explicit LogQSO(QString const& programTitle, QSettings *, QWidget *parent = 0);
  ~LogQSO();
  void initLogQSO(QString hisCall, QString hisGrid, QString mode,
                  QString rptSent, QString rptRcvd, QDateTime dateTime,
                  Radio::Frequency dialFreq, QString myCall, QString myGrid,
                  QString eqsl_username, QString eqsl_passwd, QString eqsl_nickname, bool send_to_eqsl,
                  bool noSuffix, bool toRTTY, bool dBtoComments);

public slots:
  void accept();

signals:
  void acceptQSO (QDateTime const&, QString const& call, QString const& grid
                  , Radio::Frequency dial_freq, QString const& mode
                  , QString const& rpt_sent, QString const& rpt_received
                  , QString const& tx_power, QString const& comments
                  , QString const& name);

protected:
  void hideEvent (QHideEvent *);

private:
  void loadSettings ();
  void storeSettings () const;

  QScopedPointer<Ui::LogQSO> ui;
  QSettings * m_settings;
  QString m_txPower;
  QString m_comments;
  QString m_eqslcomments;
  Radio::Frequency m_dialFreq;
  QString m_myCall;
  QString m_myGrid;
  QDateTime m_dateTime;
  bool m_send_to_eqsl;
  QString m_eqsl_username;
  QString m_eqsl_passwd;
  QString m_eqsl_nickname;
};

#endif // LogQSO_H
