// This source code file was last time modified by Igor UA3DJY on May 3rd, 2017
// All changes are shown in the patch file coming together with the full JTDX source code.

#include "about.h"

#include <QCoreApplication>
#include <QString>

#include "revision_utils.hpp"

#include "ui_about.h"
#include "moc_about.cpp"

CAboutDlg::CAboutDlg(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::CAboutDlg)
{
  ui->setupUi(this);

  ui->labelTxt->setText ("<html><h2>"
                         + QString {"JT-Reddit v17.8"}.simplified ()
                         + "</h2>\n\n"
                           "This is modified JTDX, which is based on WSJT-X, <br>"
                           "hinted decoding based on WSJT-X r6606 <br>"
                           "JT-Reddit supports JT9 and JT65a digital modes for HF <br>"
                           "amateur radio communication, focused on DXing <br>"
                           "and being shaped by community of DXers. <br>"
                           "Some parts &copy; 2017 Justin Richards, NM0J<br>"
                           "With contributions from W1GIV<br>"
                           "&copy; 2016-2017 by Igor Chernikov, UA3DJY.<br>"
                           "It is created with contributions from<br>"
                           "ES1JA, G7OED, MM0HVU, UA3ALE, US-E-12 and <br>"
                           "LY3BG family: Vytas and Rimas Kudelis.<br><br>"
                           "&copy; 2001-2016 by Joe Taylor, K1JT, with grateful <br>"
                           "acknowledgment for contributions from AC6SL, AE4JY, <br>"
                           "DJ0OT, G4KLA, G4WJS, IW3RAB, K3WYC, K9AN, KA6MAL, <br>"
                           "KA9Q, KB1ZMX, KD6EKQ, KI7MT, KK1D, ND0B, PY2SDR, <br>"
                           "VK3ACF, VK4BDJ, W4TI, W4TV, and W9MDB.<br>"
                           "<br><br>"
                           "JT-Reddit is licensed under the terms of Version 3<br>"
                           "of the GNU General Public License(GPL)<br>"
                           "<a href=\"https://www.gnu.org/licenses/gpl-3.0.txt\">"
                           "https://www.gnu.org/licenses/gpl-3.0.txt</a>");
}

CAboutDlg::~CAboutDlg()
{
}
