#This source code file was last time modified by Igor UA3DJY on March 22nd, 2017
#All changes are shown in the patch file coming together with the full JTDX source code.
# Version number components
set (WSJTX_VERSION_MAJOR 17)
set (WSJTX_VERSION_MINOR 8)
set (WSJTX_VERSION_PATCH 0)
set (WSJTX_RC 0)		 # release candidate number, comment out or zero for development versions
set (WSJTX_VERSION_IS_RELEASE 0) # set to 1 for final release build
