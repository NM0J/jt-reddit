// This source code file was last time modified by Arvo ES1JA on March 26th, 2017
// All changes are shown in the patch file coming together with the full JTDX source code.

// -*- Mode: C++ -*-
#ifndef DISPLAYTEXT_H
#define DISPLAYTEXT_H

#include <QTextEdit>
#include "logbook/logbook.h"
#include "decodedtext.h"


class DisplayText : public QTextEdit
{
    Q_OBJECT
public:
    explicit DisplayText(QWidget *parent = 0);

    void setContentFont (QFont const&);
    void insertLineSpacer(QString const&);
    void displayDecodedText(DecodedText decodedText, QString myCall, bool displayCountryName, 
                            bool displayNewDXCC, bool displayNewDXCCBand, bool displayNewDXCCBandMode,
                            bool displayNewGrid, bool displayNewGridBand, bool displayNewGridBandMode,
                            bool displayNewCall, bool displayNewCallBand, bool displayNewCallBandMode,
                            bool displayPotential, bool displayTxtColor, bool displayWorkedColor,
                            bool displayWorkedStriked, bool displayWorkedUnderlined, bool displayWorkedDontShow, 
                            QString hideContinents, LogBook logBook,
                            QColor color_CQ, QColor color_MyCall,
                            QColor color_NewDXCC, QColor color_NewDXCCBand,
                            QColor color_NewGrid, QColor color_NewGridBand,
                            QColor color_NewCall, QColor color_NewCallBand,
                            QColor color_WorkedCall, QColor color_StandardCall,
                            double dialFreq = 0, const QString app_mode = "",
                            bool beepOnNewDXCC = false, bool beepOnNewGrid = false, bool beepOnNewCall = false,
                            bool beepOnMyCall = false, bool hidefree = false, bool showcq = false,
                            bool showcq73 = false, int rx_frq = 0, QWidget* window = NULL);
    void displayTransmittedText(QString text, QString modeTx, qint32 txFreq,
                                QColor color_TxMsg, bool bFastMode);
    void displayQSY(QString text);

signals:
    void selectCallsign(bool shift, bool ctrl);

public slots:
  void appendText(QString const& text, QString const& bg = "white", QString const& color = "black", int std_type = 0, QString const& servis = "&nbsp;", QString const& cntry = "&nbsp;", bool forceBold = false, bool strikethrough = false, bool underline = false);

protected:
    void mouseDoubleClickEvent(QMouseEvent *e);

private:

    QTextCharFormat m_charFormat;
};

#endif // DISPLAYTEXT_H
